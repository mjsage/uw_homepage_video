<?php
/**
 * @file
 * uw_homepage_video.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_homepage_video_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}
